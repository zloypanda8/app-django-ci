FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN apt update 

COPY . .
 
WORKDIR /django_blog
RUN cp ?sgi.py ./Blog
RUN pip install --upgrade pip && pip install -r requirements.txt

